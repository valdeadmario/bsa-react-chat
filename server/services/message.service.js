let messages = require("../data/messages.json");

const getMessages = () => {
  if (messages) {
    return messages;
  }
  return null;
};

const getMessageById = id => {
  if (messages) {
    return messages.find(user => user.id === id);
  }
  return null;
};

const saveMessage = body => {
  if (body) {
    messages.push(body);
    return true;
  }
  return null;
};

const deleteMessage = id => {
  if (messages) {
    const idx = messages.findIndex(message => message.id === id);
    if (idx !== -1) {
      messages.splice(idx, 1);
      return true;
    }
    return null;
  }
  return null;
};

const updateMessage = (id, body) => {
  if (body) {
    messages = messages.map(message => {
      if (message.id === id) {
        return {
          id,
          ...body
        };
      }
      return message;
    });
    return true;
  }
  return null;
};

const toggleLikeMessage = id => {
  if (id) {
    messages = messages.map(message => {
      if (message.id === id) {
        return {
          ...message,
          liked: !message.liked
        };
      }
      return message;
    });
    return true;
  }
  return null;
};

module.exports = {
  getMessages,
  getMessageById,
  saveMessage,
  deleteMessage,
  updateMessage,
  toggleLikeMessage
};
