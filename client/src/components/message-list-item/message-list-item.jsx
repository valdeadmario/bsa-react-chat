import React from "react";

import defaultAvatar from "./avatar.png";
import "./message-list-item.css";

const MessageListItem = ({
  messageItem,
  currentUser,
  onDelete,
  onUpdate,
  onToggleLike
}) => {
  const { id, user, avatar, created_at, message, liked } = messageItem;
  let className = "fa fa-heart";
  if (liked) {
    className += " liked";
  }

  const renderButtons = () => {
    return (
      <React.Fragment>
        <button className="update btn  btn-sm hidden" onClick={onUpdate}>
          Edit
        </button>
        <button className="delete btn btn-sm hidden" onClick={onDelete}>
          Delete
        </button>
      </React.Fragment>
    );
  };

  const renderLike = () => (
    <button className="likes btn" onClick={onToggleLike}>
      <i className={className} />
    </button>
  );

  return (
    <li
      className={`message media ${currentUser === user ? "right" : ""}`}
      key={id}
    >
      <div className={`avatar pull-${currentUser === user ? "right" : "left"}`}>
        <img src={avatar ? avatar : defaultAvatar} alt="" />
      </div>
      <div className="body">
        <div className="text">{message}</div>
        <div className={`d-flex ${currentUser === user ? "under-mess" : ""}`}>
          <small className="ms-date d-inline">{created_at}</small>
          {currentUser === user ? renderButtons() : renderLike()}
        </div>
      </div>
    </li>
  );
};

export default MessageListItem;
