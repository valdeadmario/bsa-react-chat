let users = require("../data/users.json");

const getUsers = () => {
  if (users) {
    return users;
  }
  return null;
};

const getUserById = id => {
  if (users) {
    return users.find(user => user.id === id);
  }
  return null;
};

const saveUser = body => {
  if (body) {
    users.push(body);
    return true;
  }
  return null;
};

const deleteUser = id => {
  if (users) {
    const idx = users.findIndex(user => user.id === id);
    if (idx !== -1) {
      users.splice(idx, 1);
      return true;
    }
    return null;
  }
  return null;
};

const updateUser = (id, body) => {
  if (body) {
    const idx = users.findIndex(user => user.id === id);
    if (idx !== -1) {
      users.splice(idx, 1, { id, ...body });
      return true;
    }
  }
  return null;
};

module.exports = {
  getUsers,
  getUserById,
  saveUser,
  deleteUser,
  updateUser
};
