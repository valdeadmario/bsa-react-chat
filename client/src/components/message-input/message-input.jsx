import React, { useState } from "react";

import "./message-input.css";

const MessageInput = ({ addMessage, user, avatar }) => {
  const [label, setLabel] = useState("");

  const onSubmit = e => {
    e.preventDefault();
    if (label) {
      addMessage(label, user, avatar);
      setLabel("");
    }
  };

  return (
    <form className="d-flex message-form" onSubmit={onSubmit}>
      <input
        type="text"
        className="form-control"
        onChange={e => setLabel(e.target.value)}
        placeholder="Send your message..."
        value={label}
      />
      <button className="send btn btn-outline-danger">Send</button>
    </form>
  );
};

export default MessageInput;
