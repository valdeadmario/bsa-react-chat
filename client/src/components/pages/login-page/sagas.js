import axios from "axios";
import api from "../../../services/config/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  SEND_LOGIN,
  SEND_LOGIN_FAILURE,
  SEND_LOGIN_SUCCESS
} from "./action-types";

export function* sendLogin(action) {
  const data = action.payload;
  try {
    const response = yield call(axios.post, `${api.url}/login`, data);
    yield put({ type: SEND_LOGIN_SUCCESS, payload: { data: response.data } });
  } catch (error) {
    yield put({ type: SEND_LOGIN_FAILURE, payload: { error: error.message } });
  }
}

function* watchSendLogin() {
  yield takeEvery(SEND_LOGIN, sendLogin);
}

export default function* loginSagas() {
  yield all([watchSendLogin()]);
}
