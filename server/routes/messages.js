const express = require("express");
const router = express.Router();

const {
  getMessages,
  getMessageById,
  saveMessage,
  deleteMessage,
  updateMessage,
  toggleLikeMessage
} = require("../services/message.service");

router.get("/", function(req, res, next) {
  const result = getMessages();
  if (result) {
    res.send(result);
  } else {
    res.status(404).send("No one users...");
  }
});

router.get("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = getMessageById(id);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

router.post("/", function(req, res, next) {
  const result = saveMessage(req.body);
  if (result) {
    res.send(`User with id ${req.body.id} was added successfully`);
  } else {
    res.status(400).send("Bad data");
  }
});

router.delete("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = deleteMessage(id);
  if (result) {
    res.send(`User with id ${id} was deleted successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

router.put("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = updateMessage(id, req.body);
  if (result) {
    res.send(`User with id ${id} was updated successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

router.put("/like/:id", function(req, res, next) {
  const id = req.params.id;
  const result = toggleLikeMessage(id);
  if (result) {
    res.send(`User with id ${id} was liked successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

module.exports = router;
