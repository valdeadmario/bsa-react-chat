import { FETCH_USER } from "./action-types";

import { reduxHelper } from "../../services/redux-helpers/redux-helpers";

const initialState = {
  user: "",
  email: "",
  password: ""
};

const userReducer = reduxHelper(FETCH_USER, initialState);

export default userReducer;
