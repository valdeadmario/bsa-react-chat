import { all } from "redux-saga/effects";
import userPageSagas from "../components/user-editor/sagas";
import messagePageSagas from "../components/message-editor/sagas";
import usersSagas from "../components/user-list/sagas";
import messagesSagas from "../components/chat/sagas";
import loginSagas from "../components/pages/login-page/sagas";

export default function* rootSaga() {
  yield all([
    userPageSagas(),
    usersSagas(),
    messagePageSagas(),
    messagesSagas(),
    loginSagas()
  ]);
}
