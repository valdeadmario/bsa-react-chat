import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import "./login-page.css";

import * as actions from "./login-page-action";
import Spinner from "../../spinner";
import ErrorIndicator from "../../error-indicator/error-indicator";
import Input from "../../input";

import loginConfig from "../../../services/config/loginConfig";

const initialState = {
  email: "",
  password: ""
};

const LoginPage = ({ history, sendLogin, token, isAdmin, loading, error }) => {
  const [state, setState] = useState(initialState);

  useEffect(() => {
    if (isAdmin) {
      history.push("/users");
    } else if (token) {
      history.push("/");
    }
  }, [history, isAdmin, token]);

  const onSubmit = e => {
    e.preventDefault();
    sendLogin(state.email, state.password);
    setState(initialState);
  };

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorIndicator error={error} />;
  }

  const onChange = (e, type) => {
    const value = e.target.value;
    setState({
      ...state,
      [type]: value
    });
  };

  return (
    <section className="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div className="card card-signin my-5">
        <div className="card-body">
          <h5 className="card-title text-center">Sign In</h5>
          <form className="form-signin" onSubmit={onSubmit}>
            {loginConfig.map(({ label, type, keyword }) => {
              return (
                <Input
                  label={label}
                  type={type}
                  keyword={keyword}
                  onChange={onChange}
                  value={state[keyword]}
                  key={label}
                />
              );
            })}
            <button
              className="btn btn-lg btn-outline-danger btn-block text-uppercase"
              type="submit"
            >
              Sign in
            </button>
            <hr className="my-4" />
          </form>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    token: state.tokenReducer.data.token,
    isAdmin: state.tokenReducer.data.isAdmin,
    loading: state.tokenReducer.loading,
    error: state.tokenReducer.error
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
