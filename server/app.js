const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const messagesRouter = require("./routes/messages");
const likeRouter = require("./routes/like");

const users = require("./data/users.json");

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/messages", messagesRouter);
app.use("/like", likeRouter);

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.login);
  if (userFromReq.login === "admin" && userFromReq.password === "admin") {
    const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "24h" });
    res.status(200).json({ auth: true, isAdmin: true, token });
  } else {
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "24h" });
      res.status(200).json({
        auth: true,
        token,
        user: userInDB.user,
        avatar: userInDB.avatar
      });
    } else {
      res.status(401).json({ auth: false });
    }
  }
});

module.exports = app;
