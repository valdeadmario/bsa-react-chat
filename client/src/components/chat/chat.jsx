import React, { useEffect } from "react";
import { connect } from "react-redux";

import * as actions from "./chat-action";

import "./chat.css";

import Header from "../header";
import MessageList from "../message-list";
import MessageInput from "../message-input";
import Spinner from "../spinner";
import ErrorIndicator from "../error-indicator/error-indicator";

const Chat = ({
  history,
  token,
  isAdmin,
  user,
  avatar,
  messages,
  users,
  deleteMessage,
  fetchMessages,
  toggleLike,
  addMessage,
  loading,
  error
}) => {
  useEffect(() => {
    fetchMessages();
  }, [fetchMessages]);

  if (!token) {
    history.push("/login");
  }

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }

  return (
    <div className="chat">
      <Header
        history={history}
        countMembers={users.length}
        countMessage={messages.length}
        lastMessageTime={
          messages.length ? messages[messages.length - 1].created_at : null
        }
        isAdmin={isAdmin}
      />
      <MessageList
        messages={messages}
        history={history}
        deleteMessage={deleteMessage}
        toggleLike={toggleLike}
        currentUser={user}
      />
      <MessageInput addMessage={addMessage} user={user} avatar={avatar} />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    messages: state.chatReducer.data,
    loading: state.chatReducer.loading,
    error: state.chatReducer.error,
    users: state.usersReducer.data,
    token: state.tokenReducer.data.token,
    isAdmin: state.tokenReducer.data.isAdmin,
    avatar: state.tokenReducer.data.avatar,
    user: state.tokenReducer.data.user
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
