import { SEND_LOGIN } from "./action-types";
import { reduxHelper } from "../../../services/redux-helpers/redux-helpers";

const initialState = {
  token: "",
  isAdmin: "",
  user: "",
  avatar: ""
};

const userReducer = reduxHelper(SEND_LOGIN, initialState);

export default userReducer;
