import React from "react";

import "./header.css";

const Header = ({
  countMembers,
  countMessage,
  lastMessageTime,
  isAdmin,
  history
}) => {
  return (
    <div className="header d-flex">
      <div className="logo d-flex">
        <h1 className="name">ReactChat</h1>
        {isAdmin && (
          <button
            className="btn btn-outline-primary"
            onClick={() => history.push("/users")}
          >
            User list
          </button>
        )}
      </div>
      <div className="d-flex params">
        <h6 className="count">
          {countMembers} <i className="fa fa-user" aria-hidden="true" />
        </h6>
        <h6 className="count">
          {countMessage} <i className="fa fa-envelope" aria-hidden="true" />
        </h6>
        <h6 className="last-ms">
          <i className="fa fa-clock-o" /> {lastMessageTime}
        </h6>
      </div>
    </div>
  );
};

export default Header;
