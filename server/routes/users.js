const express = require("express");
const router = express.Router();

const {
  getUsers,
  getUserById,
  saveUser,
  deleteUser,
  updateUser
} = require("../services/user.service");

router.get("/", function(req, res, next) {
  const result = getUsers();
  if (result) {
    res.send(result);
  } else {
    res.status(404).send("No one users...");
  }
});

router.get("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = getUserById(id);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

router.post("/", function(req, res, next) {
  const result = saveUser(req.body);
  if (result) {
    res.send(`User with id ${req.body.id} was added successfully`);
  } else {
    res.status(400).send("Bad data");
  }
});

router.delete("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = deleteUser(id);
  if (result) {
    res.send(`User with id ${id} was deleted successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

router.put("/:id", function(req, res, next) {
  const id = req.params.id;
  console.log(req.body);
  const result = updateUser(id, req.body);
  if (result) {
    res.send(`User with id ${id} was updated successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});

module.exports = router;
