import { combineReducers } from "redux";
import chatReducer from "../components/chat/chat-reducers";
import messageReducer from "../components/message-editor/message-editor-reducer";
import usersReducer from "../components/user-list/user-list-reducer";
import userReducer from "../components/user-editor/user-editor-reducer";
import tokenReducer from "../components/pages/login-page/login-page-reducer";

const rootReducer = combineReducers({
  chatReducer,
  messageReducer,
  usersReducer,
  userReducer,
  tokenReducer
});

export default rootReducer;
