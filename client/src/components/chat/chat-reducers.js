import { FETCH_MESSAGES } from "./action-types";

import { reduxHelper } from "../../services/redux-helpers/redux-helpers";

const messagesReducer = reduxHelper(FETCH_MESSAGES, []);

export default messagesReducer;
