import { FETCH_USER } from "./action-types";

export const fetchUser = id => ({
  type: FETCH_USER,
  payload: {
    id
  }
});
