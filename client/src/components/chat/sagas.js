import axios from "axios";
import api from "../../services/config/api";
import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  TOGGLE_LIKE
} from "./action-types";

export function* fetchMessages() {
  try {
    const messages = yield call(axios.get, `${api.url}/messages`);
    yield put({
      type: FETCH_MESSAGES_SUCCESS,
      payload: { data: messages.data }
    });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
  const newMessage = { id: action.payload.id, ...action.payload.data };

  try {
    yield call(axios.post, `${api.url}/messages`, newMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* updateMessage(action) {
  const id = action.payload.id;
  const updatedMessage = { ...action.payload.data };

  try {
    yield call(axios.put, `${api.url}/messages/${id}`, updatedMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
  try {
    yield call(axios.delete, `${api.url}/messages/${action.payload.id}`);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* toggleLikeMessage(action) {
  try {
    yield call(axios.put, `${api.url}/like/${action.payload.id}`, true);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    yield put({
      type: FETCH_MESSAGES_FAILURE,
      payload: { error: error.message }
    });
  }
}

function* watchToggleLikeMessage() {
  yield takeEvery(TOGGLE_LIKE, toggleLikeMessage);
}

export default function* messagesSagas() {
  yield all([
    watchFetchMessages(),
    watchAddMessage(),
    watchUpdateMessage(),
    watchDeleteMessage(),
    watchToggleLikeMessage()
  ]);
}
