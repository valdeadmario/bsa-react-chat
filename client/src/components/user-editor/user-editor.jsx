import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from "./user-editor-actions";
import { addUser, updateUser } from "../user-list/user-list-actions";
import Spinner from "../spinner/spinner";
import ErrorIndicator from "../error-indicator/error-indicator";
import Input from "../input";
import userEditorCongig from "../../services/config/userEditorConfig";

const defaultUserConfig = {
  id: "",
  user: "",
  email: "",
  password: ""
};

const UserEditor = ({
  match,
  history,
  fetchUser,
  userData,
  loading,
  error,
  updateUser,
  addUser,
  isAdmin
}) => {
  const [user, setUser] = useState(defaultUserConfig);

  useEffect(() => {
    if (match.params.id) {
      fetchUser(match.params.id);
    }
  }, [fetchUser, match.params.id]);

  useEffect(() => {
    if (match.params.id) {
      setUser(userData);
    }
  }, [userData, match.params.id]);

  const onCancel = () => {
    setUser(defaultUserConfig);
    history.push("/users");
  };

  const onSave = () => {
    if (user.user && user.email && user.password) {
      if (user.id) {
        updateUser(user.id, user);
      } else {
        addUser(user);
      }
      setUser(defaultUserConfig);
      history.push("/users");
    }
  };

  const onChangeData = (e, keyword) => {
    const value = e.target.value;
    setUser({
      ...user,
      [keyword]: value
    });
  };

  if (!isAdmin) {
    history.push("/");
  }

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <ErrorIndicator error={error} />;
  }

  return (
    <section className="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div className="card card-signin my-5">
        <div className="card-body">
          <h5 className="card-title text-center">Edit User</h5>
          <div>
            {userEditorCongig.map(({ type, label, keyword }) => {
              return (
                <Input
                  type={type}
                  label={label}
                  keyword={keyword}
                  onChange={onChangeData}
                  value={user[keyword]}
                  key={label}
                />
              );
            })}
          </div>
          <div className="modal-footer">
            <button className="btn btn-secondary" onClick={onCancel}>
              Cancel
            </button>
            <button className="btn btn-warning" onClick={onSave}>
              Save
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    userData: state.userReducer.data,
    loading: state.userReducer.loading,
    error: state.userReducer.error,
    isAdmin: state.tokenReducer.data.isAdmin
  };
};

const mapDispatchToProps = {
  ...actions,
  addUser,
  updateUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEditor);
