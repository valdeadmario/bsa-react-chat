import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import "./app.css";

import Chat from "../chat";
import LoginPage from "../pages/login-page";
import UserList from "../user-list";
import UserEditor from "../user-editor";
import MessageEditor from "../message-editor";

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Chat} />
      <Route exact path="/login" component={LoginPage} />
      <Route exact path="/users" component={UserList} />
      <Route path="/message/:id" component={MessageEditor} />
      <Route exact path="/user" component={UserEditor} />
      <Route path="/user/:id" component={UserEditor} />
      <Redirect to="/" />
    </Switch>
  );
};

export default App;
