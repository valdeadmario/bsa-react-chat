import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  TOGGLE_LIKE,
  FETCH_MESSAGES
} from "./action-types";
import service from "../../services/service";

export const addMessage = (label, user, avatar) => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getNewId(),
    data: {
      user,
      avatar,
      created_at: new Date().toLocaleString(),
      message: label,
      liked: false
    }
  }
});

export const updateMessage = (id, messageData) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    data: messageData
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});

export const toggleLike = id => ({
  type: TOGGLE_LIKE,
  payload: {
    id
  }
});

export const fetchMessages = () => ({
  type: FETCH_MESSAGES
});
