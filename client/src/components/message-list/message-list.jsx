import React, { useEffect } from "react";
import { animateScroll } from "react-scroll";

import "./message-list.css";

import MessageListItem from "../message-list-item";
const MessageList = ({
  messages,
  history,
  fetchMessages,
  deleteMessage,
  toggleLike,
  currentUser
}) => {
  const scrollToBottom = () => {
    animateScroll.scrollToBottom({
      containerId: "ms-scrollbar"
    });
  };

  useEffect(() => {
    scrollToBottom();
  }, []);

  const onEdit = id => {
    history.push(`/message/${id}`);
  };

  const onToggleLike = id => {
    toggleLike(id);
  };

  const onDelete = id => {
    deleteMessage(id);
  };

  document.onkeydown = function(e) {
    if (e.key === "ArrowUp") {
      const myMessages = messages.filter(ms => ms.user === currentUser);
      if (myMessages[myMessages.length - 1]) {
        const idx = myMessages[myMessages.length - 1].id;
        onEdit(idx);
      }
    }
  };
  let date;

  const messageList = messages.map(message => {
    const { id, created_at } = message;
    let currentDate = created_at.split(" ")[0].replace(",", "");

    return (
      <React.Fragment key={id}>
        {date !== currentDate && (
          <span key={currentDate} className="line-date">
            {currentDate}
          </span>
        )}
        {(() => {
          date = currentDate;
        })()}
        <MessageListItem
          messageItem={message}
          currentUser={currentUser}
          onDelete={() => onDelete(id)}
          onUpdate={() => onEdit(id)}
          onToggleLike={() => onToggleLike(id)}
        />
      </React.Fragment>
    );
  });

  return (
    <div
      className="message-list listview lv-message"
      id="ms-scrollbar"
      tabIndex="0"
    >
      {messageList}
    </div>
  );
};

export default MessageList;
