const express = require("express");
const router = express.Router();

const { toggleLikeMessage } = require("../services/message.service");

router.put("/:id", function(req, res, next) {
  const id = req.params.id;
  const result = toggleLikeMessage(id);
  if (result) {
    res.send(`User with id ${id} was liked successfully`);
  } else {
    res.status(400).send(`${id} no one user with this id`);
  }
});
module.exports = router;
