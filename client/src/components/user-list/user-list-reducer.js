import { FETCH_USERS } from "./action-types";

import { reduxHelper } from "../../services/redux-helpers/redux-helpers";

const usersReducer = reduxHelper(FETCH_USERS, []);

export default usersReducer;
