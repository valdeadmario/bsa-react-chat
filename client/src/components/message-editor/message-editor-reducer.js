import { FETCH_MESSAGE } from "./action-types";

import { reduxHelper } from "../../services/redux-helpers/redux-helpers";

const initialState = {
  id: "",
  user: "",
  avatar: "",
  message: "",
  liked: "",
  created_at: ""
};

const userReducer = reduxHelper(FETCH_MESSAGE, initialState);

export default userReducer;
